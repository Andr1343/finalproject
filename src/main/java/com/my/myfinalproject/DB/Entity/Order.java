package com.my.myfinalproject.DB.Entity;

public class Order {
    int orderId;
    String serviceName;
    int masterId;
    int clientId;
    int orderPrice;
    String orderDate;
    String orderTime;
    String orderStatus;
    String orderPayment;
    String orderRating;
    public Order(String serviceName, int masterId, int clientId, int orderPrice, String orderDate,
                            String orderTime, String orderStatus, String orderPayment, String orderRating){
        this.serviceName = serviceName;
        this.masterId = masterId;
        this.clientId = clientId;
        this.orderPrice = orderPrice;
        this.orderDate = orderDate;
        this.orderTime = orderTime;
        this.orderStatus = orderStatus;
        this.orderPayment = orderPayment;
        this.orderRating = orderRating;
    }

    public Order(int orderId, String serviceName, int masterId, int clientId, int orderPrice, String orderDate,
                 String orderTime, String orderStatus, String orderPayment, String orderRating){
        this.orderId = orderId;
        this.serviceName = serviceName;
        this.masterId = masterId;
        this.clientId = clientId;
        this.orderPrice = orderPrice;
        this.orderDate = orderDate;
        this.orderTime = orderTime;
        this.orderStatus = orderStatus;
        this.orderPayment = orderPayment;
        this.orderRating = orderRating;
    }


    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public int getMasterId() {
        return masterId;
    }

    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(int orderPrice) {
        this.orderPrice = orderPrice;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(String orderTime) {
        this.orderTime = orderTime;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderPayment() {
        return orderPayment;
    }

    public void setOrderPayment(String orderPayment) {
        this.orderPayment = orderPayment;
    }

    public String getOrderRating() {
        return orderRating;
    }

    public void setOrderRating(String orderRating) {
        this.orderRating = orderRating;
    }
}
