package com.my.myfinalproject.DB.Entity;

public class User {
    private int Id;
    private String FirstName;
    private String LastName;
    private int Age;
    private String Sex;
    private String Phone;
    private String Email;
    private String Password;
    private String Role;
    private int Rating;

    public User(String firstName, String lastName, int age, String sex, String phone, String email, String password, String role) {
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Age = age;
        this.Sex = sex;
        this.Phone = phone;
        this.Email = email;
        this.Password = password;
        this.Role = role;
    }

    public User(int Id, String firstName, String lastName, int age, String sex, String phone, String email, String password, String role) {
        this.Id = Id;
        this.FirstName = firstName;
        this.LastName = lastName;
        this.Age = age;
        this.Sex = sex;
        this.Phone = phone;
        this.Email = email;
        this.Password = password;
        this.Role = role;
    }


    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public int getAge() {
        return Age;
    }

    public void setAge(int age) {
        Age = age;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getRole() {
        return Role;
    }

    public void setRole(String role) {
        Role = role;
    }

    public String getSex() {
        return Sex;
    }

    public void setSex(String sex) {
        Sex = sex;
    }

    public int getRating(){
        return Rating;
    }

    public void setRating(int rating){
        this.Rating = rating;
    }

}
