package com.my.myfinalproject.DB.Entity;

public class Service {
    private int Id;
    private String Name;
    private int HairdresserId;
    private int Price;

    public Service(String name, int hairdresserId, int price) {
        this.Name = name;
        this.HairdresserId = hairdresserId;
        this.Price = price;
    }

    public Service(int id, String name, int hairdresserId, int price) {
        this.Id = id;
        this.Name = name;
        this.HairdresserId = hairdresserId;
        this.Price = price;
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return Name;
    }

    public int getHairdresserId() {
        return HairdresserId;
    }

    public int getPrice() {
        return Price;
    }
}
