package com.my.myfinalproject.DB.DaoImpl;

import org.apache.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/* Represent Connection Pool witch controls all operations with DB */
public class ConnectionPool {
    private static final Logger logger = Logger.getLogger(ConnectionPool.class) ;

    private ConnectionPool(){
        //private constructor
    }

    private static ConnectionPool instance = null;

    /* Instance of Connection Pool */
    public static ConnectionPool getInstance(){
        if (instance==null)
            instance = new ConnectionPool();
        return instance;
    }

    /*
    @return Connection
    @throws SQLException
     */
    public Connection getConnection() throws SQLException{
        DataSource ds ;
        Context initContext;
        Context envContext;
        Connection c ;
        try {
            initContext = new InitialContext();
            envContext = (Context) initContext.lookup("java:comp/env");
            ds = (DataSource) envContext.lookup("jdbc/FinalDB");
            c = ds.getConnection();
        } catch(NamingException | SQLException e) {
            logger.error("Connection Pool error: " + e);
            throw new SQLException(e) ;
        }
        return c;
    }
}
