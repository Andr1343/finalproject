package com.my.myfinalproject.DB.DaoImpl;

import com.my.myfinalproject.DB.Dao.ServiceDao;
import com.my.myfinalproject.DB.Entity.Service;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ServiceDaoImpl implements ServiceDao {

    private static final Logger logger = Logger.getLogger(ServiceDaoImpl.class);


    @Override
    public boolean addService(Service addService) throws SQLException {

        if(!checkServiceByName(addService.getName(), addService.getHairdresserId())){

            Connection connection = null;
            PreparedStatement prp = null;

            try {
                connection = ConnectionPool.getInstance().getConnection();
                prp = connection.prepareStatement("INSERT INTO services VALUES(default, ?, ?, ?)");
                prp.setString(1, addService.getName());
                prp.setInt(2, addService.getHairdresserId());
                prp.setInt(3, addService.getPrice());
                prp.execute();
            } catch (SQLException e) {
                logger.error("DataBase error : " + e);
                throw new SQLException(e);
            } finally {
                try {
                    if (prp != null) { prp.close(); }
                    if (connection != null) { connection.close(); }
                } catch (SQLException e) {
                    logger.error("Close connection error : " + e);
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean checkServiceByName(String serviceName, int hairDresserId) throws SQLException {
        Service newService = getServiceByName(serviceName, hairDresserId);
        return newService != null;
    }

    @Override
    public Service getServiceByName(String serviceName, int hairDresserId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        Service service = null;
        int hasService = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE ServiceName = ? AND `Service - MasterId` = ?");
            prp.setString(1, serviceName);
            prp.setInt(2, hairDresserId);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasService++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("Service - MasterId"),
                        rs.getInt("Price"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasService > 0) {
            return service;
        } else {
            return null;
        }
    }

    @Override
    public List<Service> getServices() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Service> serviceList = new ArrayList<>();
        Service service;
        int hasService = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE `Service - MasterId` = ?");
            prp.setInt(1, 1);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasService++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("Service - MasterId"),
                        rs.getInt("Price"));
                serviceList.add(service);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasService > 0) {
            return serviceList;
        } else {
            return null;
        }
    }

    @Override
    public List<Service> getServicesByName(String serviceName) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Service> servicesList = new ArrayList<>();
        Service service;
        int hasMaster = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE ServiceName = ? AND `Service - MasterId` != 1");
            prp.setString(1, serviceName);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasMaster++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("Service - MasterId"),
                        rs.getInt("Price"));
                servicesList.add(service);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasMaster > 0) {
            return servicesList;
        } else {
            return null;
        }
    }

    @Override
    public List<Service> getServicesByMasterId(int hairDresserId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Service> serviceList = new ArrayList<>();
        Service service;
        int hasService = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE `Service - MasterId` = ?");
            prp.setInt(1, hairDresserId);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasService++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("Service - MasterId"),
                        rs.getInt("Price"));
                serviceList.add(service);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasService > 0) {
            return serviceList;
        } else {
            return null;
        }
    }
}
