package com.my.myfinalproject.DB.DaoImpl;

import com.my.myfinalproject.DB.Dao.UserDao;
import com.my.myfinalproject.DB.Entity.User;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private static final Logger logger = Logger.getLogger(UserDaoImpl.class);

    @Override
    public boolean addUser(User addUser) throws SQLException {

        User userEmail = getUserByEmail(addUser.getEmail());
        User userPhone = getUserByPhone(addUser.getPhone());

        if(userPhone == null && userEmail == null){
            Connection connection = null;
            PreparedStatement prp = null;
            try {
                connection = ConnectionPool.getInstance().getConnection();
                prp = connection.prepareStatement("INSERT INTO users VALUES(default, ?, ?, ?, ?, ?, ?, ?, ?, null)");
                prp.setString(1, addUser.getFirstName());
                prp.setString(2, addUser.getLastName());
                prp.setInt(3, addUser.getAge());
                prp.setString(4, addUser.getSex());
                prp.setString(5, addUser.getPhone());
                prp.setString(6, addUser.getEmail());
                prp.setString(7, addUser.getPassword());
                prp.setString(8, addUser.getRole());
                prp.execute();
            } catch (SQLException e) {
                logger.error("DataBase error : " + e);
                throw new SQLException(e);
            } finally {
                try {
                    if (prp != null) { prp.close(); }
                    if (connection != null) { connection.close(); }
                } catch (SQLException e) {
                    logger.error("Close connection error : " + e);
                }
            }

            return true;
        } else {
            logger.info("Current email or phone is already used : " + addUser.getEmail() + ", " + addUser.getPhone());
            return false;
        }
    }

    @Override
    public boolean updateUser(User updateUser) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE users SET FirstName = ?, LastName = ?, Age = ?, Sex = ?, Password = ? WHERE Email = ? ");
            prp.setString(1, updateUser.getFirstName());
            prp.setString(2, updateUser.getLastName());
            prp.setInt(3, updateUser.getAge());
            prp.setString(4, updateUser.getSex());
            prp.setString(5, updateUser.getPassword());
            prp.setString(6, updateUser.getEmail());
            prp.executeUpdate();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return true;
    }

    @Override
    public boolean checkLogIn(String email, String password) throws SQLException {

        if (getUserByEmail(email) == null) {
            return false;
        } else {
            User checkUser = getUserByEmail(email);

            if (checkUser.getPassword().equals(password)) {
                return true;
            } else {
                logger.info(checkUser.getEmail() + " - Wrong Password");
                return false;
            }
        }
    }

    @Override
    public User getUserById(int userId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        User user = null;
        int hasUser = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Id = ?");
            prp.setInt(1, userId);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasUser++;
                user = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasUser > 0) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    public User getUserByEmail(String email) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        User user = null;
        int hasUser = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Email = ?");
            prp.setString(1, email);
            rs = prp.executeQuery();
            while (rs.next()) {
                hasUser++;
                user = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasUser > 0) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    public User getUserByPhone(String phone) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        User user = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Phone = ?");
            prp.setString(1, phone);
            rs = prp.executeQuery();
            while (rs.next()) {
                user = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return user;
    }

    @Override
    public List<User> getUsers() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<User> usersList = new ArrayList<>();
        User user;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Role != ?");
            prp.setString(1, "admin");
            rs = prp.executeQuery();

            while (rs.next()) {
                user = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
                usersList.add(user);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return usersList;
    }

}
