package com.my.myfinalproject.DB.DaoImpl;

import com.my.myfinalproject.DB.Dao.MasterDao;
import com.my.myfinalproject.DB.Entity.Service;
import com.my.myfinalproject.DB.Entity.User;
import org.apache.log4j.Logger ;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
Represent a class with method for works with DB table
 */
public class MasterDaoImpl implements MasterDao {

    private static final Logger logger = Logger.getLogger(MasterDaoImpl.class) ;

    /*
    @return true if master was created, or false if master cant be created
    @throws SQLException
     */
    @Override
    public boolean addMaster(int userId, String userRole) throws SQLException {
        
        Connection connection = null;
        PreparedStatement prp = null;
                
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE users SET Role = ? WHERE Id = ?");
            prp.setString(1, userRole);
            prp.setInt(2, userId);
            prp.executeUpdate();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }
        return true;
    }

    /*
    @return true if master have already add service, or false if master haven`t gor this service
    @throws SQLException
     */
    @Override
    public boolean checkServiceByMaster(String serviceName, User master) throws SQLException {
        Service newServiceForMaster = getServiceByMaster(serviceName, master);
        return newServiceForMaster != null;
    }

    /*
    Calculate and set rating for each master
     */
    @Override
    public void setMastersRating() throws SQLException {
        List<User> masters = getMasters();
        for(int i = 0; i < masters.size(); i++) {
            int masterId = masters.get(i).getId();
            int masterRating = 0;
            int ordersRating = 0;

            Connection connection = null;
            PreparedStatement prp = null;
            PreparedStatement prp1 = null;
            ResultSet rs = null;

            int hasService = 0;
            try {
                connection = ConnectionPool.getInstance().getConnection();
                prp = connection.prepareStatement("SELECT * FROM orders WHERE `Order - MasterId` = ?");
                prp.setInt(1,masterId);
                rs = prp.executeQuery();
                while (rs.next()) {
                    hasService++;
                    ordersRating = ordersRating + rs.getInt("OrderRating");
                }
                if (ordersRating != 0 && hasService != 0) {
                    masterRating = ordersRating / hasService;
                }

                if(masterRating != 0) {
                    prp1 = connection.prepareStatement("UPDATE users SET Rating = ? WHERE Id = ?");
                    prp1.setInt(1, masterRating);
                    prp1.setInt(2, masterId);
                    prp1.executeUpdate();
                }
            } catch (SQLException e) {
                logger.error("DataBase error : " + e);
                throw new SQLException(e);
            } finally {
                try {
                    if (rs != null) { rs.close(); }
                    if (prp != null) { prp.close(); }
                    if (prp1 != null) { prp1.close(); }
                    if (connection != null) { connection.close(); }
                } catch (SQLException e) {
                    logger.error("Close connection error : " + e);
                }
            }
        }
    }

    /*
    @return service for master
     */
    @Override
    public Service getServiceByMaster(String serviceName, User master) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        Service service = null;
        int hasService = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE ServiceName = ? AND `Service - MasterId` = ?");
            prp.setString(1, serviceName);
            prp.setInt(2,master.getId());
            rs = prp.executeQuery();
            while (rs.next()) {
                hasService++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("MasterId"),
                        rs.getInt("Price"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasService > 0) {
            return service;
        } else {
            return null;
        }
    }

    /*
    @return list of Users who have status "master"
     */
    @Override
    public List<User> getMasters() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<User> mastersList = new ArrayList<>();
        User master;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Role = ?");
            prp.setString(1, "master");
            rs = prp.executeQuery();
            while (rs.next()) {
                master = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
                master.setRating(rs.getInt("Rating"));
                mastersList.add(master);
            }
            prp.close();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return mastersList;
    }

    /*
    @return list of users which have status "master" and sort by name
     */
    @Override
    public List<User> getMastersSortByName() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<User> mastersList = new ArrayList<>();
        User master;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Role = ? ORDER BY LastName, FirstName");
            prp.setString(1, "master");
            rs = prp.executeQuery();
            while (rs.next()) {
                master = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
                master.setRating(rs.getInt("Rating"));
                mastersList.add(master);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return mastersList;
    }

    /*
    @return list of users which have status "master" and sort by rating
     */
    @Override
    public List<User> getMastersSortByRating() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<User> mastersList = new ArrayList<>();
        User master;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM users WHERE Role = ? ORDER BY Rating, LastName, FirstName");
            prp.setString(1, "master");
            rs = prp.executeQuery();
            while (rs.next()) {
                master = new User(rs.getInt("Id"),
                        rs.getString("FirstName"),
                        rs.getString("LastName"),
                        rs.getInt("Age"),
                        rs.getString("Sex"),
                        rs.getString("Phone"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Role"));
                master.setRating(rs.getInt("Rating"));
                mastersList.add(master);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return mastersList;
    }

    /*
    @return list of services for current master
     */
    @Override
    public List<Service> getServicesByMaster(User master) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Service> serviceList = new ArrayList<>();
        Service service;
        int hasService = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE `Service - MasterId` = ?");
            prp.setInt(1, master.getId());
            rs = prp.executeQuery();
            while (rs.next()) {
                hasService++;
                service = new Service(rs.getInt("Id"),
                        rs.getString("ServiceName"),
                        rs.getInt("MasterId"),
                        rs.getInt("Price"));
                serviceList.add(service);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasService > 0) {
            return serviceList;
        } else {
            return null;
        }
    }

    /*
    @return list of users who can do current service
     */
    @Override
    public List<User> getMastersByService(Service service) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<User> mastersList = new ArrayList<>();
        User master;
        int hasMaster = 0;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM services WHERE ServiceName = ? AND `Service - MasterId` != 1");
            prp.setString(1, service.getName());
            rs = prp.executeQuery();
            while (rs.next()) {
                hasMaster++;
                int id = rs.getInt("Service - MasterId");
                master = new UserDaoImpl().getUserById(id);
                mastersList.add(master);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if (hasMaster > 0) {
            return mastersList;
        } else {
            return null;
        }
    }
}
