package com.my.myfinalproject.DB.DaoImpl;

import com.my.myfinalproject.DB.Dao.OrderDao;
import com.my.myfinalproject.DB.Entity.Order;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/*
Represent a class with method for works with DB table
 */
public class OrderDaoImpl implements OrderDao {

    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class);

    /*
        @return true if order was created, or false if order cant be created
        @throws SQLException
         */
    @Override
    public boolean addOrder(Order order) throws SQLException {

        if(checkOrderTime(order)) {

            Connection connection = null;
            PreparedStatement prp = null;

            try {
                connection = ConnectionPool.getInstance().getConnection();
                prp = connection.prepareStatement("INSERT INTO orders VALUES(default, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                prp.setString(1, order.getServiceName());
                prp.setInt(2, order.getMasterId());
                prp.setInt(3, order.getClientId());
                prp.setInt(4, order.getOrderPrice());
                prp.setString(5, order.getOrderDate());
                prp.setString(6, order.getOrderTime());
                prp.setString(7, order.getOrderStatus());
                prp.setString(8, order.getOrderPayment());
                prp.setString(9, order.getOrderRating());
                prp.execute();
            } catch (SQLException e) {
                logger.error("DataBase error : " + e);
                throw new SQLException(e);
            } finally {
                try {
                    if (prp != null) { prp.close(); }
                    if (connection != null) { connection.close(); }
                } catch (SQLException e) {
                    logger.error("Close connection error : " + e);
                }
            }

            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean updateOrderDate(String newOrderDate, String newOrderTime, int orderId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE orders SET OrderDate = ?, OrderTime = ? WHERE OrderNumber = ?");
            prp.setString(1, newOrderDate);
            prp.setString(2, newOrderTime);
            prp.setInt(3, orderId);
            prp.execute();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return true;
    }

    @Override
    public boolean updateOrderStatus(String newStatus, int orderId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE orders SET OrderStatus = ? WHERE OrderNumber = ?");
            prp.setString(1, newStatus);
            prp.setInt(2, orderId);
            prp.execute();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }
        return true;
    }

    @Override
    public boolean updateOrderPayment(String newPayment, int orderId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE orders SET OrderPayment = ? WHERE OrderNumber = ?");
            prp.setString(1, newPayment);
            prp.setInt(2, orderId);
            prp.execute();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return true;
    }

    @Override
    public boolean updateOrderRating(int newRating, int orderId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;

        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("UPDATE orders SET OrderRating = ? WHERE OrderNumber = ?");
            prp.setInt(1, newRating);
            prp.setInt(2, orderId);
            prp.execute();
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return true;
    }

    @Override
    public boolean checkOrderTime(Order orderCheck) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        Order order = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE OrderDate = ? AND `Order - MasterId` = ? AND OrderTime = ?");
            prp.setString(1, orderCheck.getOrderDate());
            prp.setInt(2, orderCheck.getMasterId());
            prp.setString(3, orderCheck.getOrderTime());
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        if(order != null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public List<Order> getOrders() throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC ");
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }

    @Override
    public List<Order> getOrdersMaster(int masterId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE `Order - MasterId` = ? ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC");
            prp.setInt(1, masterId);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }

    @Override
    public List<Order> getOrdersClient(int clientId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE `Order - ClientId` = ? ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC");
            prp.setInt(1, clientId);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }

    @Override
    public Order getOrderByNumber(int orderNumber) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        Order order = null;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE OrderNumber = ?");
            prp.setInt(1, orderNumber);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return order;
    }

    @Override
    public List<Order> getOrdersByDate(String orderDate) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE OrderDate = ? ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC");
            prp.setString(1, orderDate);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }

    @Override
    public List<Order> getOrdersByDateMaster(String orderDate, int masterId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE OrderDate = ? AND `Order - MasterId` = ? ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC");
            prp.setString(1, orderDate);
            prp.setInt(2, masterId);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }

    @Override
    public List<Order> getOrdersByDateClient(String orderDate, int clientId) throws SQLException {

        Connection connection = null;
        PreparedStatement prp = null;
        ResultSet rs = null;

        List<Order> ordersList = new ArrayList<>();
        Order order;
        try {
            connection = ConnectionPool.getInstance().getConnection();
            prp = connection.prepareStatement("SELECT * FROM orders WHERE OrderDate = ? AND `Order - ClientId` = ? ORDER BY OrderDate DESC, OrderTime DESC, OrderNumber DESC");
            prp.setString(1, orderDate);
            prp.setInt(2, clientId);
            rs = prp.executeQuery();

            while (rs.next()) {
                order = new Order(rs.getInt("OrderNumber"),
                        rs.getString("ServiceName"),
                        rs.getInt("Order - MasterId"),
                        rs.getInt("Order - ClientId"),
                        rs.getInt("OrderPrice"),
                        rs.getString("OrderDate"),
                        rs.getString("OrderTime"),
                        rs.getString("OrderStatus"),
                        rs.getString("OrderPayment"),
                        rs.getString("OrderRating"));
                ordersList.add(order);
            }
        } catch (SQLException e) {
            logger.error("DataBase error : " + e);
            throw new SQLException(e);
        } finally {
            try {
                if (rs != null) { rs.close(); }
                if (prp != null) { prp.close(); }
                if (connection != null) { connection.close(); }
            } catch (SQLException e) {
                logger.error("Close connection error : " + e);
            }
        }

        return ordersList;
    }
}
