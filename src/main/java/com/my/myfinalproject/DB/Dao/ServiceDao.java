package com.my.myfinalproject.DB.Dao;

import com.my.myfinalproject.DB.Entity.Service;

import java.sql.SQLException;
import java.util.List;

public interface ServiceDao {

    boolean addService(Service service) throws SQLException;
    boolean checkServiceByName(String serviceName, int hairDresserId) throws SQLException;
    Service getServiceByName(String serviceName, int hairDresserId) throws SQLException;
    List<Service> getServices() throws SQLException;
    List<Service> getServicesByName(String serviceName) throws SQLException;
    List<Service> getServicesByMasterId(int hairDresserId) throws SQLException;

}
