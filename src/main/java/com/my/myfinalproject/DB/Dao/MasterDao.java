package com.my.myfinalproject.DB.Dao;

import com.my.myfinalproject.DB.Entity.Service;
import com.my.myfinalproject.DB.Entity.User;

import java.sql.SQLException;
import java.util.List;

public interface MasterDao {
    boolean addMaster(int userId, String userRole) throws SQLException;
    boolean checkServiceByMaster(String serviceName, User master) throws SQLException;
    void setMastersRating() throws SQLException;
    Service getServiceByMaster(String serviceName, User master) throws SQLException;
    List<User> getMasters() throws SQLException;
    List<User> getMastersSortByName() throws SQLException;
    List<User> getMastersSortByRating() throws SQLException;
    List<Service> getServicesByMaster(User master) throws SQLException;
    List<User> getMastersByService(Service service) throws SQLException;
}
