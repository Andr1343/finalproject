package com.my.myfinalproject.DB.Dao;

import com.my.myfinalproject.DB.Entity.Order;

import java.sql.SQLException;
import java.util.List;

public interface OrderDao {
    boolean addOrder(Order order) throws SQLException;
    boolean updateOrderDate(String newOrderDate, String newOrderTime, int orderId) throws SQLException;
    boolean updateOrderStatus(String newStatus, int orderId) throws SQLException;
    boolean updateOrderPayment(String newPayment, int orderId) throws SQLException;
    boolean updateOrderRating(int newRating, int orderId) throws SQLException;
    boolean checkOrderTime(Order order) throws SQLException;
    List<Order> getOrders() throws SQLException;
    List<Order> getOrdersMaster(int masterId) throws SQLException;
    List<Order> getOrdersClient(int clientId) throws SQLException;
    Order getOrderByNumber(int orderNumber) throws SQLException;
    List<Order> getOrdersByDate(String orderDate) throws SQLException;
    List<Order> getOrdersByDateMaster(String orderDate, int masterId) throws SQLException;
    List<Order> getOrdersByDateClient(String orderDate, int clientId) throws SQLException;

}
