package com.my.myfinalproject.DB.Dao;


import com.my.myfinalproject.DB.Entity.User;

import java.sql.SQLException;
import java.util.List;

public interface UserDao {
    boolean addUser(User addUser) throws SQLException;
    boolean updateUser(User updateUser) throws SQLException;
    boolean checkLogIn(String email, String password) throws SQLException;
    User getUserById(int userId) throws SQLException;
    User getUserByEmail(String email) throws SQLException;
    User getUserByPhone(String phone) throws SQLException;
    List<User> getUsers() throws SQLException;

    }
