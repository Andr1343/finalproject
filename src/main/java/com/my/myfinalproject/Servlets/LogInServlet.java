package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.UserDaoImpl;
import com.my.myfinalproject.DB.Entity.User;
import org.apache.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/login-servlet")
public class LogInServlet extends HttpServlet {

    private static final Logger logger = Logger.getLogger(LogInServlet.class);

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String lang = (String) req.getSession().getAttribute("lang");
        req.getSession().invalidate();
        req.setAttribute("lang", lang);
        resp.sendRedirect("./");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        req.getSession().invalidate();

        String email = req.getParameter("email");
        String password = req.getParameter("password");

        UserDaoImpl ud = new UserDaoImpl();
        User logInUser = null;
        try {
            logInUser = ud.getUserByEmail(email);
        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }


        if (logInUser == null) {
            logger.info("User with email : " + email + " - did not found.");
            req.getSession().setAttribute("error", "email");
            resp.sendRedirect("Error.jsp");
        } else {
            if (!logInUser.getPassword().equals(password)) {
                logger.info("User with email : " + email + " - use wrong password.");
                req.getSession().setAttribute("error", "password");
                resp.sendRedirect("Error.jsp");
            } else {
                HttpSession session = req.getSession();
                session.setAttribute("user", logInUser);
                logger.info("User with email : " + email + " - logged in.");
                resp.sendRedirect("./");
            }
        }
    }
}