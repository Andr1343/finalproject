package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.UserDaoImpl;
import com.my.myfinalproject.DB.Entity.*;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/registration-servlet")
public class RegistrationServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        System.out.println("doGet - RegistrationServlet");
        resp.sendRedirect("MyProfile.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws IOException {

        System.out.println("doPost - RegistrationServlet");

//        ArrayList<String> parameterNames = new ArrayList<String>();
//        Enumeration enumeration = req.getParameterNames();
//        while (enumeration.hasMoreElements()) {
//            String parameterName = (String) enumeration.nextElement();
//            parameterNames.add(parameterName);
//        }

        try {
            if (req.getSession().getAttribute("user") != null) {

                System.out.println("user != null");

                HttpSession session = req.getSession();
                User userUpdate = (User) session.getAttribute("user");
                session.removeAttribute("user");

                userUpdate.setFirstName(req.getParameter("firstName"));
                userUpdate.setLastName(req.getParameter("lastName"));
                userUpdate.setAge(Integer.parseInt(req.getParameter("ages")));
                userUpdate.setSex(req.getParameter("sex"));
                userUpdate.setPassword(req.getParameter("password"));

                UserDaoImpl userDao = new UserDaoImpl();
                userDao.updateUser(userUpdate);

                session.setAttribute("user", userUpdate);
                resp.sendRedirect("./");
            } else {

                System.out.println("user == null");

                req.getSession().invalidate();

                String firstName = req.getParameter("firstName");
                String lastName = req.getParameter("lastName");
                int age = Integer.parseInt(req.getParameter("ages"));
                String sex = req.getParameter("sex");
                String phone = req.getParameter("phoneNumber");
                String email = req.getParameter("email");
                String password = req.getParameter("password");
                String role = "client";

                User newUser = new User(firstName, lastName, age, sex, phone, email, password, role);
                UserDaoImpl ud = new UserDaoImpl();

                if (ud.addUser(newUser)) {
                    resp.sendRedirect("LogIn.html");
                } else {
                    req.getSession().setAttribute("error", "isUsed");
                    resp.sendRedirect("Error.jsp");
                }
            }

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }
}