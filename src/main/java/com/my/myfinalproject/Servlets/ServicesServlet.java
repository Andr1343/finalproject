package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.ServiceDaoImpl;
import com.my.myfinalproject.DB.DaoImpl.UserDaoImpl;
import com.my.myfinalproject.DB.Entity.Service;
import com.my.myfinalproject.DB.Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/services-servlet")
public class ServicesServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        System.out.println("doGet - ServiceServlet");

        try {
            String path = "/index.jsp";
            ServiceDaoImpl serviceDao = new ServiceDaoImpl();
            UserDaoImpl userDao = new UserDaoImpl();
            List<Service> serviceList = serviceDao.getServices();
            int page;

            if(req.getParameter("page") == null) {
                page = 1;
            } else {
                page = Integer.parseInt(req.getParameter("page"));
            }

            List<Service> newList;
            if(serviceList.size() < page*5) {
                newList = serviceList.subList((page - 1) * 5, serviceList.size());
            } else {
                newList = serviceList.subList((page - 1) * 5, page * 5);
            }
            req.setAttribute("servicesList", newList);


            if (req.getSession().getAttribute("user") != null) {
                HttpSession session = req.getSession();
                User user = (User) session.getAttribute("user");
                if (user.getRole().equals("master")) {
                    session.setAttribute("servicesList", serviceList);

                    List<Service> serviceMasterList = serviceDao.getServicesByMasterId(user.getId());
                    session.setAttribute("servicesMasterList", serviceMasterList);
                }
            } else {
                String lang = (String) req.getSession().getAttribute("lang");
                req.getSession().invalidate();
                req.getSession().setAttribute("lang", lang);
            }

            if (req.getParameter("masterId") != null) {
                int masterId = Integer.parseInt(req.getParameter("masterId"));
                List<Service> serviceMasterList = serviceDao.getServicesByMasterId(masterId);
                if (serviceMasterList != null) {
                    User master = userDao.getUserById(masterId);
                    req.setAttribute("masterOfServicesList", master);
                    req.setAttribute("servicesList", serviceMasterList);
                } else {
                    throw new RuntimeException();
                }
            }

            getServletContext().getRequestDispatcher(path).forward(req, resp);

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        } catch (RuntimeException e) {
            resp.sendRedirect("masters-servlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        System.out.println("doPost - ServiceServlet");

        try {
            int masterId;
            String serviceName = req.getParameter("serviceName");
            HttpSession session = req.getSession();
            User user = (User) session.getAttribute("user");

            if (user.getRole().equals("admin")) {
                masterId = 1;
            } else {
                masterId = user.getId();
            }

            int servicePrice = Integer.parseInt(req.getParameter("servicePrice"));

            Service service = new Service(serviceName, masterId, servicePrice);
            ServiceDaoImpl serviceDao = new ServiceDaoImpl();

            if (serviceDao.addService(service)) {
                resp.sendRedirect("services-servlet");
            } else {
                session.setAttribute("error", "Current service already exist");
                resp.sendRedirect("Error.jsp");
            }
        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }
}
