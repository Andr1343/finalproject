package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.MasterDaoImpl;
import com.my.myfinalproject.DB.DaoImpl.ServiceDaoImpl;
import com.my.myfinalproject.DB.Entity.Service;
import com.my.myfinalproject.DB.Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/masters-servlet")
public class MastersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        System.out.println("doGet - MastersServlet");

        try {

            String path = "/index.jsp";
            List<User> masterList;
            MasterDaoImpl masterDao = new MasterDaoImpl();
            masterDao.setMastersRating();

            if (req.getParameter("sortBy") != null && req.getParameter("sortBy").equals("Name")) {
                masterList = masterDao.getMastersSortByName();
            } else if (req.getParameter("sortBy") != null && req.getParameter("sortBy").equals("Rating")) {
                masterList = masterDao.getMastersSortByRating();
            } else if (req.getParameter("serviceName") != null) {
                List<Service> serviceOfMasters;
                ServiceDaoImpl serviceDao = new ServiceDaoImpl();

                Service service = serviceDao.getServiceByName(req.getParameter("serviceName"), 1);

                if(service != null) {
                    serviceOfMasters = serviceDao.getServicesByName(service.getName());
                    req.setAttribute("serviceOfMaster", serviceOfMasters);
                } else {
                    throw new RuntimeException();
                }

                masterList = masterDao.getMastersByService(service);
            } else {
                masterList = masterDao.getMasters();
            }

            req.setAttribute("mastersList", masterList);
            getServletContext().getRequestDispatcher(path).forward(req, resp);

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        } catch (RuntimeException e) {
            resp.sendRedirect("services-servlet");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        System.out.println("doPost - MastersServlet");

        try {

            int userId = Integer.parseInt(req.getParameter("userId"));
            String userRole = req.getParameter("userRole");

            MasterDaoImpl masterDao = new MasterDaoImpl();
            masterDao.addMaster(userId, userRole);

            resp.sendRedirect("users-servlet");

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }
}
