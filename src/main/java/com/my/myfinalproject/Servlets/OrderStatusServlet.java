package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.OrderDaoImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/ordersStatus-servlet")
public class OrderStatusServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String lang = req.getParameter("changeLang");
        req.getSession().setAttribute("lang", lang);
        resp.sendRedirect("./");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        System.out.println("doPost - OrdersStatusServlet");

        try {
            OrderDaoImpl orderDao = new OrderDaoImpl();

            if (req.getParameter("changeStatus") != null) {
                String newStatus = req.getParameter("changeStatus");
                int orderId = Integer.parseInt(req.getParameter("orderId"));
                orderDao.updateOrderStatus(newStatus, orderId);
            } else if (req.getParameter("changeRating") != null) {
                int newRating = Integer.parseInt(req.getParameter("changeRating"));
                int orderId = Integer.parseInt(req.getParameter("orderId"));
                orderDao.updateOrderRating(newRating, orderId);

            } else if (req.getParameter("changePayment") != null) {
                String newPayment = req.getParameter("changePayment");
                int orderId = Integer.parseInt(req.getParameter("orderId"));
                orderDao.updateOrderPayment(newPayment, orderId);
            }

            resp.sendRedirect("orders-servlet");

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }
}
