package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.MasterDaoImpl;
import com.my.myfinalproject.DB.DaoImpl.OrderDaoImpl;
import com.my.myfinalproject.DB.DaoImpl.ServiceDaoImpl;
import com.my.myfinalproject.DB.DaoImpl.UserDaoImpl;
import com.my.myfinalproject.DB.Entity.Order;
import com.my.myfinalproject.DB.Entity.Service;
import com.my.myfinalproject.DB.Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/orders-servlet")
public class OrdersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("doGet - OrderServlet ");

        try {
            String path = "/index.jsp";
            UserDaoImpl userDao = new UserDaoImpl();
            List<User> userList = userDao.getUsers();
            userList.add(userDao.getUserById(1));
            req.setAttribute("usersList", userList);

            MasterDaoImpl masterDao = new MasterDaoImpl();
            List<User> masterList = masterDao.getMasters();
            req.setAttribute("mastersList", masterList);

            System.out.println(LocalDateTime.now().getYear() + "-" + LocalDateTime.now().getMonthValue() + "-" + LocalDateTime.now().getDayOfMonth());
            String orderDate = req.getParameter("date");
            User user = (User) req.getSession().getAttribute("user");
            List<Order> orderList;
            OrderDaoImpl orderDao = new OrderDaoImpl();

            if (user.getRole().equals("client")) {
                if (orderDate == null || orderDate.equals("")) {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrdersClient(user.getId());
                } else {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrdersByDateClient(orderDate, user.getId());
                }
            } else if (user.getRole().equals("master")) {
                System.out.println("doGet - OrderServlet - " + user.getRole());
                if (orderDate == null || orderDate.equals("")) {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrdersMaster(user.getId());
                } else {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrdersByDateMaster(orderDate, user.getId());
                }
            } else {
                if (orderDate == null || orderDate.equals("")) {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrders();
                } else {
                    System.out.println("orderDate - " + orderDate);
                    orderList = orderDao.getOrdersByDate(orderDate);
                }
            }

            if (orderDate != null && orderDate.equals("")) {
                resp.sendRedirect("orders-servlet");
            } else {
                req.setAttribute("ordersList", orderList);
                getServletContext().getRequestDispatcher(path).forward(req, resp);
            }

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {

        System.out.println("doPost - OrdersServlet");

        try {
            MasterDaoImpl masterDao = new MasterDaoImpl();
            ServiceDaoImpl serviceDao = new ServiceDaoImpl();
            UserDaoImpl userDao = new UserDaoImpl();
            User orderClient = (User) req.getSession().getAttribute("user");

            if (req.getParameter("masterOrder") != null) {
                System.out.println("OrdersServlet - #doPost - masterOrder");
                List<User> masterList = masterDao.getMasters();
                req.setAttribute("mastersList", masterList);
                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            } else if (req.getParameter("serviceOrder") != null) {
                System.out.println(" OrdersServlet - #doPost - serviceOrder");
                List<Service> serviceList = serviceDao.getServices();
                req.setAttribute("servicesList", serviceList);
                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            } else if (req.getParameter("masterId") != null) {
                System.out.println("OrdersServlet - #doPost - masterId");
                int masterId = Integer.parseInt(req.getParameter("masterId"));
                User master = userDao.getUserById(masterId);
                req.setAttribute("master", master);

                List<Service> serviceList = serviceDao.getServicesByMasterId(masterId);
                req.setAttribute("servicesForMasterList", serviceList);
                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            } else if (req.getParameter("serviceName") != null) {
                System.out.println("OrdersServlet - #doPost - serviceName");
                String orderServiceName = req.getParameter("serviceName");
                Service orderService = serviceDao.getServiceByName(orderServiceName, 1);
                req.setAttribute("orderService", orderService);

                List<Service> priceList = serviceDao.getServicesByName(orderServiceName);
                req.setAttribute("pricesList", priceList);

                List<User> masterList = masterDao.getMastersByService(orderService);
                req.setAttribute("mastersForServiceList", masterList);
                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            } else if (req.getParameter("orderNumber") != null) {
                System.out.println("OrdersServlet - #doPost - orderNumber");

                OrderDaoImpl orderDao = new OrderDaoImpl();
                Order order = orderDao.getOrderByNumber(Integer.parseInt(req.getParameter("orderNumber")));
                req.setAttribute("orderNumber", order.getOrderId());

                User master = userDao.getUserById(order.getMasterId());
                req.setAttribute("master", master);

                List<Service> serviceList = new ArrayList<>();
                serviceList.add(serviceDao.getServiceByName(order.getServiceName(), order.getMasterId()));
                req.setAttribute("servicesForMasterList", serviceList);
                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            }


            if (req.getParameter("orderDate") != null) {
                System.out.println("OrdersServlet - #doPost - orderDate" + " " + req.getParameter("orderDate"));

                if (req.getParameter("finalOrderNumber") != null) {
                    req.setAttribute("orderNumber", req.getParameter("finalOrderNumber"));
                }

                int masterId = Integer.parseInt(req.getParameter("finalMasterId"));
                User master = userDao.getUserById(masterId);
                req.setAttribute("master", master);

                String orderServiceName = req.getParameter("finalServiceName");
                Service orderService = serviceDao.getServiceByName(orderServiceName, masterId);
                req.setAttribute("service", orderService);

                String orderDate = req.getParameter("orderDate");
                req.setAttribute("finalOrderDate", orderDate);

                OrderDaoImpl orderDao = new OrderDaoImpl();
                List<Order> orderByDateMaster = orderDao.getOrdersByDateMaster(orderDate, masterId);
                req.setAttribute("ordersByDateMaster", orderByDateMaster);

                getServletContext().getRequestDispatcher("/NewOrder.jsp").forward(req, resp);
            } else if (req.getParameter("orderTime") != null) {
                System.out.println("OrdersServlet - #doPost - orderTime");

                OrderDaoImpl orderDao = new OrderDaoImpl();
                int orderMasterId = Integer.parseInt(req.getParameter("finalMasterId"));
                String orderServiceName = req.getParameter("finalServiceName");

                int orderClientId = orderClient.getId();
                Service service = serviceDao.getServiceByName(orderServiceName, orderMasterId);

                int orderPrice = service.getPrice();
                String orderDate = req.getParameter("finalOrderDate");
                String orderTime = req.getParameter("orderTime");

                if (req.getParameter("finalOrderNumber") != null) {
                    System.out.println("Order Update");

                    if (orderDao.updateOrderDate(orderDate, orderTime, Integer.parseInt(req.getParameter("finalOrderNumber")))) {
                        System.out.println("Order was update");
                        resp.sendRedirect("orders-servlet");
                    } else {
                        req.getSession().setAttribute("error", "Current order can`t be update");
                        resp.sendRedirect("Error.jsp");
                    }
                } else {
                    Order newOrder = new Order(orderServiceName, orderMasterId, orderClientId, orderPrice, orderDate, orderTime, "Reserved", "Not Paid", "0");

                    if (orderDao.addOrder(newOrder)) {
                        System.out.println("Order was created");
                        resp.sendRedirect("orders-servlet");
                    } else {
                        resp.sendRedirect("Error.jsp");
                    }
                }
            }

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }
}
