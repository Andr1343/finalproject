package com.my.myfinalproject.Servlets;

import com.my.myfinalproject.DB.DaoImpl.UserDaoImpl;
import com.my.myfinalproject.DB.Entity.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/users-servlet")
public class UsersServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        System.out.println("doGet - UsersServlet");

        try {
            String path = "/index.jsp";
            UserDaoImpl userDao = new UserDaoImpl();
            List<User> userList = userDao.getUsers();
            req.setAttribute("usersList", userList);
            getServletContext().getRequestDispatcher(path).forward(req, resp);

        } catch (SQLException e) {
            resp.sendRedirect("Error.jsp");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("doPost - UsersServlet");
        super.doPost(req, resp);
    }
}
