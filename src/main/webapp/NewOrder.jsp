<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="message"/>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Andriy Aftanas">
    <meta name="generator" content="">
    <title>New Order</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/NewOrder.css" rel="stylesheet">
</head>

<body class="text-center">
<main class="form-logIn w-100 m-auto">

    <c:if test="${not empty requestScope.get('servicesList')}">
        <form onsubmit="return checkOrderService(this)" action="orders-servlet" method="post">
            <img class="mb-4Logo" src="img/Logo4.jpg" alt="" width="299" height="350">
            <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
                <div class="form-floating">
                    <select class="form-control" name="serviceName" id="serviceName1">
                        <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseService"/> </option>
                        <c:forEach var="service" items="${requestScope.get('servicesList')}">
                            <option value="${service.getName()}">${service.getName()}</option>
                        </c:forEach>
                    </select>
                    <label for="serviceName1"> <fmt:message key="label.nameService"/> </label>
                </div>
            <div id="serviceError" style="color: red"></div>
            <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.nextStep"/> </button>
        </form>
    </c:if>

    <c:if test="${not empty requestScope.get('mastersList')}">
        <form onsubmit="return checkOrderHairDresser(this)" action="orders-servlet" method="post">
            <img class="mb-4Logo" src="img/Logo.jpg" alt="" width="299" height="330">
            <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
                <div class="form-floating">
                    <select class="form-control" name="masterId" id="masterName1">
                        <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseHairDresser"/> </option>
                        <c:forEach var="master" items="${requestScope.get('mastersList')}">
                            <option value="${master.getId()}" <c:if test="${sessionScope.get('user').getId() == master.getId()}"> disabled="disabled" </c:if> >${master.getFirstName()} ${master.getLastName()}</option>
                        </c:forEach>
                    </select>
                    <label for="masterName1"> <fmt:message key="label.hairDresserName"/> </label>
                </div>
            <div id="masterError" style="color: red"></div>
            <br>
            <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.nextStep"/> </button>
        </form>
    </c:if>

    <c:if test="${not empty requestScope.get('servicesForMasterList')}">
        <form onsubmit="return checkOrderByService(this)" action="orders-servlet" method="post">
            <img class="mb-4Logo" src="img/Logo.jpg" alt="" width="299" height="350">
            <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
                <c:if test="${not empty requestScope.get('orderNumber')}">
                    <div class="form-floating">
                        <select class="form-control" name="finalOrderNumber" id="orderNumber">
                            <option value="${requestScope.get('orderNumber')}" selected="selected">${requestScope.get('orderNumber')}</option>
                        </select>
                        <label for="masterId"> <fmt:message key="label.orderNumber"/> </label>
                    </div>
                </c:if>
                <div class="form-floating">
                    <select class="form-control" name="finalMasterId" id="masterId">
                        <option value="${requestScope.get('master').getId()}" selected="selected">${requestScope.get('master').getFirstName()} ${requestScope.get('master').getLastName()}</option>
                    </select>
                    <label for="masterId"> <fmt:message key="label.hairDresserName"/> </label>
                </div>
                <div class="form-floating">
                    <select class="form-control" name="finalServiceName" id="serviceName2">
                        <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseService"/> </option>
                        <c:forEach var="service" items="${requestScope.get('servicesForMasterList')}">
                            <option value="${service.getName()}">${service.getName()}  -  ${service.getPrice()} <fmt:message key="label.priceValue"/> </option>
                        </c:forEach>
                    </select>
                    <label for="serviceName2"> <fmt:message key="label.nameService"/> </label>
                </div>
                <div class="form-floating">
                    <input class="form-control" value="${now}" type="date" name="orderDate" id="orderD">
                    <label for="orderD"> <fmt:message key="label.date"/> </label>
                </div>
            <div id="masterDateError" style="color: red"></div>
            <br>
            <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.nextStep"/> </button>
        </form>
    </c:if>

    <c:if test="${not empty requestScope.get('mastersForServiceList')}">
        <form onsubmit="return checkOrderByHairDresser(this)" action="orders-servlet" method="post">
            <img class="mb-4Logo" src="img/Logo4.jpg" alt="" width="299" height="350">
            <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
            <div class="form-floating">
                <select class="form-control" name="finalServiceName" id="serviceName">
                    <option value="${requestScope.get('orderService').getName()}" selected="selected">${requestScope.get('orderService').getName()}</option>
                </select>
                <label for="serviceName"> <fmt:message key="label.nameService"/> </label>
            </div>
            <div class="form-floating">
                <select class="form-control" name="finalMasterId" id="masterIds">
                    <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseHairDresser"/> </option>
                    <c:forEach var="master" items="${requestScope.get('mastersForServiceList')}">
                        <option value="${master.getId()}" <c:if test="${sessionScope.get('user').getId() == master.getId()}"> disabled="disabled" </c:if> >${master.getFirstName()} ${master.getLastName()}  -  <c:forEach var="servicePrice" items="${requestScope.get('pricesList')}"><c:if test="${master.getId() == servicePrice.getHairdresserId()}">${servicePrice.getPrice()}</c:if></c:forEach> <fmt:message key="label.priceValue"/> </option>
                    </c:forEach>
                </select>
                <label for="masterIds"> <fmt:message key="label.hairDresserName"/> </label>
            </div>
            <div class="form-floating">
                <input class="form-control" value="${now}" type="date" name="orderDate" id="orderDt">
                <label for="orderDt"> <fmt:message key="label.date"/> </label>
            </div>
            <div id="serviceDateError" style="color: red"></div>
            <br>
            <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.nextStep"/> </button>
        </form>
    </c:if>


    <c:if test="${not empty requestScope.get('finalOrderDate')}">
        <form onsubmit="return checkOrderTime(this)" action="orders-servlet" method="post">
            <img class="mb-4Logo" src="img/Logo3.jpg" alt="" width="299" height="350">
            <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
            <c:if test="${not empty requestScope.get('orderNumber')}">
                <div class="form-floating">
                    <select class="form-control" name="finalOrderNumber" id="finalOrderNumber">
                        <option value="${requestScope.get('orderNumber')}" selected="selected">${requestScope.get('orderNumber')}</option>
                    </select>
                    <label for="masterId"> <fmt:message key="label.orderNumber"/> </label>
                </div>
            </c:if>
            <div class="form-floating">
                <select class="form-control" name="finalMasterId" id="finalMasterId">
                    <option value="${requestScope.get('master').getId()}" selected="selected">${requestScope.get('master').getFirstName()} ${requestScope.get('master').getLastName()}</option>
                </select>
                <label for="finalMasterId"> <fmt:message key="label.nameHairDresser"/> </label>
            </div>
            <div class="form-floating">
                <select class="form-control" name="finalServiceName" id="finalServiceName">
                    <option value="${requestScope.get('service').getName()}" selected="selected">${requestScope.get('service').getName()}  -  ${requestScope.get('service').getPrice()} <fmt:message key="label.priceValue"/></option>
                </select>
                <label for="finalServiceName"> <fmt:message key="label.nameService"/> </label>
            </div>
            <div class="form-floating">
                <select class="form-control" name="finalOrderDate" id="finalOrderD">
                    <option value="${requestScope.get('finalOrderDate')}" selected="selected">${requestScope.get('finalOrderDate')}</option>
                </select>
                <label for="finalOrderD"> <fmt:message key="label.date"/> </label>
            </div>
            <div class="form-floating">
                <select class="form-control" name="orderTime" id="orderT">
                    <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseTime"/> </option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '10:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="10:00">10:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '11:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="11:00">11:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '12:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="12:00">12:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '13:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="13:00">13:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '14:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="14:00">14:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '15:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="15:00">15:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '16:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="16:00">16:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '17:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="17:00">17:00</option>
                    <option <c:forEach var="order" items="${requestScope.get('ordersByDateMaster')}"> <c:if test="${order.getOrderTime() == '18:00:00' and order.getOrderStatus() != 'Canceled'}"> disabled="disabled" </c:if> </c:forEach> value="18:00">18:00</option>
                </select>
                <label for="orderT"> <fmt:message key="label.time"/> </label>
            </div>
            <div id="masterTimeError" style="color: red"></div>
            <br>
            <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.makeNewOrder"/> </button>
        </form>
    </c:if>

    <button onclick="window.location.href = 'orders-servlet'" class="btn btn-outline-warning"> <fmt:message key="label.cancel"/> </button>
</main>

<script charset="UTF-8" type="text/javascript" src="js/NewOrder.js" ></script>

</body>
</html>
