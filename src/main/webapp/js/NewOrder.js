
function checkOrderService(el) {

    let serviceName = el.serviceName.value;
    let fail = "";

    if( serviceName == null || serviceName == "" || serviceName == "choose") {
        fail = "Виберіть послугу";
    }

    if(fail != "") {
        document.getElementById('serviceError').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}



function checkOrderHairDresser(el) {

    let masterId = el.masterId.value;

    let fail = "";

    if( masterId == null || masterId == "" || masterId == "choose") {
        fail = "Виберіть майстра";
    }

    if(fail != "") {
        document.getElementById('masterError').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}



function checkOrderByService(el){

    let masterId = el.finalMasterId.value;
    let serviceName = el.finalServiceName.value;
    let orderDate = new Date(el.orderDate.value);
    let date = new Date();
    date.setHours(23,59,1, 1);

    console.log(orderDate);

    let fail = "";

    if( serviceName == "" || masterId == "" || serviceName == "choose" || masterId == "choose" || el.orderDate.value == null || el.orderDate.value == "") {
        fail = "Заповніть всі дані";
    } else if (date.getTime() > orderDate.getTime()) {
        fail = "Некоректна дата";
    }


    if(fail != "") {
        document.getElementById('masterDateError').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}

function checkOrderByHairDresser(el){

    let masterId = el.finalMasterId.value;
    let serviceName = el.finalServiceName.value;
    let orderDate = new Date(el.orderDate.value);
    let date = new Date();
    date.setHours(23,59,1, 1);

    console.log(orderDate);

    let fail = "";

    if( serviceName == "" || masterId == "" || serviceName == "choose" || masterId == "choose" || el.orderDate.value == null || el.orderDate.value == "") {
        fail = "Заповніть всі дані";
    } else if (date.getTime() > orderDate.getTime()) {
        fail = "Некоректна дата";
    }


    if(fail != "") {
        document.getElementById('serviceDateError').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}



function checkOrderTime(el) {
    let time = el.orderTime.value;

    let fail = "";

    if( time == "choose" ) {
        fail = "Заповніть всі дані";
    }


    if(fail != "") {
        document.getElementById('masterTimeError').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}