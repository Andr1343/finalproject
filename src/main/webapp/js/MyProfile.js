
function checkProfile(el){

    var firstName = el.firstName.value;
    var lastName = el.lastName.value;
    var age = el.ages.value;
    var pass = el.password.value;
    var confPass = el.confirmPassword.value;

    const EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;

    var fail = "";

    if( firstName == "" || lastName == "" || pass == "" || confPass == "" ) {
        fail = "Заповніть всі дані";
    } else if( firstName.length <= 2 || lastName.length <= 2 ) {
        fail = "Некоректне ім'я";
    } else if( age < 16) {
        fail = "Некоректний вік";
    } else if( pass !== confPass ) {
        fail = "Паролі не співпадають";
    } else if (pass.split("&").length > 1) {
        fail = "Некоректний пароль";
    }

    if(fail != "") {
        document.getElementById('error').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}