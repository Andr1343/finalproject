
function checkLogIn(el){

    var email = el.email.value;
    var pass = el.password.value;

    const EMAIL_REGEXP = /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/iu;

    var fail = "";

    if( email === "" || pass === "") {
        fail = "Заповніть всі дані";
    } else if (EMAIL_REGEXP.test(email) === false) {
        fail = "Некоректний Email";
    } else if (pass.split("&", "?", "!").length > 1) {
        fail = "Некоректний пароль";
    }

    if(fail != "") {
        document.getElementById('error').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}