
function checkNewService(el){

    let serviceName = el.serviceName.value;
    let servicePrice = el.servicePrice.value;

    let fail = "";

    if( serviceName === "" || servicePrice === "") {
        fail = "Заповніть всі дані";
    }

    if(fail !== "") {
        document.getElementById('error').innerHTML = fail;
        console.log(fail);
        return false;
    } else {
        return true;
    }
}


function checkService(el){

    let serviceName = el.serviceName.value;
    let servicePrice = el.servicePrice.value;

    let fail = "";

    if( serviceName === "" || serviceName == null || serviceName === 'choose' || servicePrice === "") {
        fail = "Заповніть всі дані";
    }

    if(fail !== "") {
        document.getElementById('error1').innerHTML = fail;
        return false;
    } else {
        return true;
    }
}