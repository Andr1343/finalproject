
function checkStatus(el, status){

    var newStatus = el.changeStatus.value;
    var oldStatus = status;

    if(newStatus == oldStatus) {
        return false;
    }

    return true;
}

function checkPayment(el, payment){

    var newPayment = el.changePayment.value;
    var oldPayment = payment;

    if(newPayment == oldPayment) {
        return false;
    }

    return true;
}


document.addEventListener('DOMContentLoaded', () => {

    const getSort = ({ target }) => {
        const order = (target.dataset.order = -(target.dataset.order || -1));
        const index = [...target.parentNode.cells].indexOf(target);
        const collator = new Intl.Collator(['en', 'ru'], { numeric: true });
        const comparator = (index, order) => (a, b) => order * collator.compare(
            a.children[index].innerHTML,
            b.children[index].innerHTML
        );

        for(const tBody of target.closest('table').tBodies)
            tBody.append(...[...tBody.rows].sort(comparator(index, order)));

        for(const cell of target.parentNode.cells)
            cell.classList.toggle('sorted', cell === target);
    };

    document.querySelectorAll('.table_sort thead').forEach(tableTH => tableTH.addEventListener('click', () => getSort(event)));

});