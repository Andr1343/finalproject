<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="message"/>

<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Andriy Aftanas">
  <meta name="generator" content="">
  <title>MyFinalProject</title>

  <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/headers/">

  <link href="css/bootstrap.min.css" rel="stylesheet">

  <style>

    .bi {
      vertical-align: -.125em;
      fill: currentColor;
    }

  </style>

  <style>

    table {
      margin: auto;
      width: auto;
      table-layout: fixed;
      border-collapse: collapse;
      border: 2px solid black;
      text-align: left;
      caption-side: top;
    }

    caption {
      text-align: center;
      padding: 30px 10px 10px 10px;
      font-size: xxx-large;
      font-weight: bolder;
      font-family: Gabriola, serif;
      color: black;
    }

    .mastersTable thead, .servicesTable thead, .variantTable thead {
      font-family: "Times New Roman", cursive;
      font-size: x-large;
      font-weight: bolder;
      text-align: center;
    }

    .mastersTable th, .servicesTable th, .variantTable th {
      border: 2px solid black;
      padding: 1px;
    }

    .mastersTable td, .servicesTable td, .variantTable td {
      border: 2px solid black;
      padding: 30px;
      font-family: Georgia, serif;
      font-weight: bolder;
    }


    .table_sort thead {
      border: 2px solid black;
      cursor: pointer;
    }
    .table_sort th {
      border: 2px solid black;
      font-family: "Times New Roman", cursive;
      font-weight: bolder;
      font-size: larger;
      padding: 5px 15px 5px 15px;
      text-align: center;
    }

    .table_sort td {
      border: 2px solid black;
      padding: 5px 10px 5px 10px;
      font-family: Georgia, serif;
    }

    th.sorted[data-order="1"],
    th.sorted[data-order="-1"] {
      position: relative;
    }

    th.sorted[data-order="1"]::after,
    th.sorted[data-order="-1"]::after {
      right: 8px;
      position: absolute;
    }

    th.sorted[data-order="-1"]::after {
      content: "▼"
    }

    th.sorted[data-order="1"]::after {
      content: "▲"
    }

  </style>

  <!-- Custom styles for this template -->
  <link href="css/headers.css" rel="stylesheet">
</head>

<body>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
  <symbol id="bootstrap" viewBox="0 0 118 94">
    <title>Bootstrap</title>
    <path fill-rule="evenodd" clip-rule="evenodd" d="M24.509 0c-6.733 0-11.715 5.893-11.492 12.284.214 6.14-.064 14.092-2.066 20.577C8.943 39.365 5.547 43.485 0 44.014v5.972c5.547.529 8.943 4.649 10.951 11.153 2.002 6.485 2.28 14.437 2.066 20.577C12.794 88.106 17.776 94 24.51 94H93.5c6.733 0 11.714-5.893 11.491-12.284-.214-6.14.064-14.092 2.066-20.577 2.009-6.504 5.396-10.624 10.943-11.153v-5.972c-5.547-.529-8.934-4.649-10.943-11.153-2.002-6.484-2.28-14.437-2.066-20.577C105.214 5.894 100.233 0 93.5 0H24.508zM80 57.863C80 66.663 73.436 72 62.543 72H44a2 2 0 01-2-2V24a2 2 0 012-2h18.437c9.083 0 15.044 4.92 15.044 12.474 0 5.302-4.01 10.049-9.119 10.88v.277C75.317 46.394 80 51.21 80 57.863zM60.521 28.34H49.948v14.934h8.905c6.884 0 10.68-2.772 10.68-7.727 0-4.643-3.264-7.207-9.012-7.207zM49.948 49.2v16.458H60.91c7.167 0 10.964-2.876 10.964-8.281 0-5.406-3.903-8.178-11.425-8.178H49.948z"></path>
  </symbol>

</svg>


<main>
  <h1 class="visually-hidden">MyFinalProject</h1>

  <header class="p-3 text-bg-dark">
    <div class="container">
      <div class="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
        <a href="./" class="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none">
          <svg class="bi me-2" width="40" height="32" role="img" aria-label="MyFinalProject"><use xlink:href="#bootstrap"/></svg>
        </a>

        <ul class="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0">
          <li><a href="./" class="nav-link px-2 text-secondary"><fmt:message key="label.main"/></a></li>
          <li><a href="masters-servlet" class="nav-link px-2 text-white"><fmt:message key="label.masters"/></a></li>
          <li><a href="services-servlet" class="nav-link px-2 text-secondary"><fmt:message key="label.services"/></a></li>
          <li><a href="#" class="nav-link px-2 text-white"><fmt:message key="label.contacts"/></a></li>
          <c:if test="${not empty sessionScope.get('user')}">
            <li><a href="orders-servlet" class="nav-link px-2 text-secondary"><fmt:message key="label.orders"/></a></li>
          </c:if>
          <c:if test="${sessionScope.get('user').getRole() == 'admin'}">
            <li><a href="users-servlet" class="nav-link px-2 text-white"><fmt:message key="label.users"/></a></li>
          </c:if>
        </ul>

        <div class="text-end">
          <c:if test="${empty sessionScope.get('user')}">
            <form>
            <button onclick="window.location.href = 'LogIn.html'" type="button" class="btn btn-outline-light me-2"><fmt:message key="label.logIn"/></button>
            <button onclick="window.location.href = 'Registration.html'" type="button" class="btn btn-warning"><fmt:message key="label.sigUp"/></button>

            <c:if test="${sessionScope.get('lang') == 'uk'}"><button formaction="ordersStatus-servlet" name="changeLang" value="en" class="btn btn-outline-success">English</button></c:if>
            <c:if test="${sessionScope.get('lang') == 'en'}"><button formaction="ordersStatus-servlet" name="changeLang" value="uk" class="btn btn-outline-success">Українська</button></c:if>
            </form>
          </c:if>
          <c:if test="${not empty sessionScope.get('user')}">
            <form action="login-servlet" method="get">
              <fmt:message key="label.hello"/> ${sessionScope.get('user').getFirstName()}
              <button formaction="registration-servlet" class="btn btn-outline-light me-2"><fmt:message key="label.myProfile"/></button>
              <button class="btn btn-outline-warning"><fmt:message key="label.logOut"/></button>

              <c:if test="${sessionScope.get('lang') == 'uk'}"><button formaction="ordersStatus-servlet" name="changeLang" value="en" class="btn btn-outline-success">English</button></c:if>
              <c:if test="${sessionScope.get('lang') == 'en'}"><button formaction="ordersStatus-servlet" name="changeLang" value="uk" class="btn btn-outline-success">Українська</button></c:if>
            </form>
          </c:if>
        </div>
      </div>
    </div>
  </header>
  </main>

      <div>

        <c:if test="${empty requestScope}">
          <div>
            <table class="variantTable">

              <caption> Variant </caption>

              <thead>
                <tr>
                  <th> Beauty salon </th>
                </tr>
              </thead>

              <tbody>
                <tr>
                  <td>
                The system implements the work schedule of beauty salon employees. There are roles: <br>
                Guest, Client, Administrator, Hairdresser.<br><br>
                The guest can see the catalog of services and the list of hairdressers taking into account
                sorting:<br>
                - by the name;<br>
                - by rating<br><br>
                can filter:<br>
                - by certain hairdresser;<br>
                - by services.<br><br>
                The client (authorized user) can sign up for a specific service provided by the hairdresser
                and for a specific time slot.<br><br>
                The administrator can:<br>
                - view customer orders and change the selected time slot;<br>
                - cancel order;<br>
                - accept payment for the service.<br><br>
                The hairdresser sees his schedule (busy and free time slots) and marks the execution of the
                order.<br><br>
                After providing services, the Client leaves feedback. The offer to leave feedback comes to
                the Client&#39;s e-mail the day after the service is provided.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </c:if>

        <c:if test="${not empty requestScope.get('mastersList') and empty requestScope.get('ordersList') and empty requestScope.get('usersList')}">
          <div>
            <table class="mastersTable">

              <caption><fmt:message key="label.listHairdressers"/> <c:if test="${not empty requestScope.get('serviceOfMaster')}"> - ${requestScope.get('serviceOfMaster').get(0).getName()}</c:if></caption>

              <tbody>
              <form action="masters-servlet" method="get">
                <select name="sortBy">
                  <option value="Name"><fmt:message key="label.nameHairDresser"/></option>
                  <option value="Rating"><fmt:message key="label.rating"/></option>
                </select>
                <button class="btn btn-outline-dark" type="submit"><fmt:message key="label.sortBy"/></button>
              </form>
                <c:forEach var="masterList" items="${requestScope.get('mastersList')}">
                  <tr>
                      <th><img src="img/Logo.jpg" width="200" height="220" alt="Master photo"> </th>
                      <td>
                        <fmt:message key="label.nameHairDresser"/>: ${masterList.getFirstName()} ${masterList.getLastName()}; <br>
                        <fmt:message key="label.age"/>: ${masterList.getAge()} <fmt:message key="label.age2"/>; <br>
                        <fmt:message key="label.rating"/>: ${masterList.getRating()} <br> <br>
                        <c:forEach var="service" items="${requestScope.get('serviceOfMaster')}">
                          <c:if test="${masterList.getId() == service.getHairdresserId()}">
                            <fmt:message key="label.price"/> - ${service.getPrice()} <fmt:message key="label.priceValue"/>
                          </c:if>
                        </c:forEach>
                        <form action="services-servlet" method="get">
                          <button class="btn btn-outline-success btn-sm" type="submit" name="masterId" value="${masterList.getId()}"><fmt:message key="label.showServices"/></button>
                        </form>
                      </td>
                  </tr>
                </c:forEach>
              </tbody>
            </table>
          </div>
        </c:if>

        <c:if test="${sessionScope.get('user').getRole() != 'client' and sessionScope.get('user').getRole() != null and not empty requestScope.get('servicesList')}">
          <div class="text-end p-3">
            <button type="button" onclick="window.location.href = 'NewService.jsp'" class="btn btn-outline-info"><fmt:message key="label.addNewService"/></button>
          </div>
        </c:if>

        <c:if test="${not empty requestScope.get('servicesList')}">
          <div>
            <table class="servicesTable">

              <caption><fmt:message key="label.listServices"/><c:if test="${not empty requestScope.get('masterOfServicesList')}"> - ${requestScope.get('masterOfServicesList').getFirstName()} ${requestScope.get('masterOfServicesList').getLastName()}</c:if></caption>

              <tbody>
              <c:forEach var="serviceList" items="${requestScope.get('servicesList')}">
                <tr>
                  <th><img src="img/Logo4.jpg" width="200" height="220" alt="Service photo"> </th>
                  <td>
                    <fmt:message key="label.nameService"/>: ${serviceList.getName()}; <br>
                    <fmt:message key="label.price"/>: <c:if test="${empty requestScope.get('masterOfServicesList')}"><fmt:message key="label.from"/></c:if> ${serviceList.getPrice()} <fmt:message key="label.priceValue"/>; <br> <br>
                    <form action="masters-servlet" method="get">
                      <button class="btn btn-outline-success btn-sm" type="submit" name="serviceName" value="${serviceList.getName()}"><fmt:message key="label.showHairDressers"/></button>
                    </form>
                  </td>
                </tr>
              </c:forEach>
              </tbody>
            </table>
          </div>
          <br>
          <div style="text-align: center">
            <form action="services-servlet" method="get">
              <button class="btn btn-outline-success btn-sm" name="page" value="1" <c:if test="${requestScope.get('page') == '1'}"> disabled="disabled" </c:if>>1</button>
              <button class="btn btn-outline-success btn-sm" name="page" value="2" <c:if test="${requestScope.get('page') == '2'}"> disabled="disabled" </c:if>>2</button>
              <button class="btn btn-outline-success btn-sm" name="page" value="3" <c:if test="${requestScope.get('page') == '3'}"> disabled="disabled" </c:if>>3</button>
            </form>
          </div>
          <br>
        </c:if>



        <c:if test="${not empty requestScope.get('mastersList') and not empty requestScope.get('usersList')}">

          <div class="text-end p-3">
            <form method="post" action="orders-servlet">
              <button type="submit" name="masterOrder" value="1"  class="btn btn-outline-info"> <fmt:message key="label.makeOrderHairDresser"/> </button>
              <button type="submit" name="serviceOrder" value="1" class="btn btn-outline-info"> <fmt:message key="label.makeOrderService"/> </button>
            </form>
          </div>

          <div>
            <table class="table_sort">

              <caption>
                <fmt:message key="label.listOrders"/>
              </caption>

              <thead>
                <tr>
                  <th> <fmt:message key="label.numberOrder"/> </th>
                  <th> <fmt:message key="label.nameService"/> </th>
                  <th> <fmt:message key="label.hairDresser"/> </th>
                  <th> <fmt:message key="label.client"/> </th>
                  <th> <fmt:message key="label.price"/> </th>
                  <th> <fmt:message key="label.date"/> </th>
                  <th> <fmt:message key="label.time"/> </th>
                  <th> <fmt:message key="label.status"/> </th>
                  <th> <fmt:message key="label.payment"/> </th>
                  <th> <fmt:message key="label.ratingOrder"/> </th>
                  <th>
                    <form action="orders-servlet" method="get">
                      <input type="date" name="date">
                      <button type="submit"> <fmt:message key="label.dateCheck"/> </button>
                    </form>
                  </th>
                </tr>
              </thead>

              <c:if test="${not empty requestScope.get('ordersList')}">
                <tbody>
                  <c:forEach var="order" items="${requestScope.get('ordersList')}" varStatus="vr">
                    <tr>
                      <td> ${order.getOrderId()} </td>
                      <td> ${order.getServiceName()} </td>
                        <c:forEach var="masterList" items="${requestScope.get('mastersList')}">
                          <c:if test="${masterList.getId() == order.getMasterId()}">
                            <td> ${masterList.getFirstName()} ${masterList.getLastName()}</td>
                          </c:if>
                        </c:forEach>
                        <c:forEach var="user" items="${requestScope.get('usersList')}">
                          <c:if test="${user.getId() == order.getClientId()}">
                            <td> ${user.getFirstName()} ${user.getLastName()}</td>
                          </c:if>
                        </c:forEach>
                      <td> ${order.getOrderPrice()} </td>
                      <td> ${order.getOrderDate()} </td>
                      <td> ${order.getOrderTime()} </td>
                      <td> ${order.getOrderStatus()} </td>
                      <c:if test="${sessionScope.get('user').getRole() != 'admin'}">
                        <td> ${order.getOrderPayment()} </td>
                      </c:if>
                      <c:if test="${sessionScope.get('user').getRole() == 'admin'}">
                        <td>
                          <form id="payedForm" onsubmit="return checkPayment(this, '${order.getOrderPayment()}')" action="ordersStatus-servlet" method="post">
                            <select name="changePayment">
                              <option value="Paid" <c:if test="${order.getOrderPayment() == 'Paid'}">disabled="disabled" selected="selected"</c:if>><fmt:message key="label.paid"/></option>
                              <option value="Not Paid" <c:if test="${order.getOrderPayment() == 'Not Paid'}">disabled="disabled" selected="selected"</c:if>><fmt:message key="label.notPaid"/></option>
                            </select>
                            <button class="btn btn-outline-dark" type="submit"  name="orderId" value="${order.getOrderId()}"><fmt:message key="label.change"/></button>
                          </form>
                        </td>
                      </c:if>
                      <td> ${order.getOrderRating()} </td>
                      <td>
                        <c:if test="${sessionScope.get('user').getId() == order.getClientId() and order.getOrderStatus() == 'Finished' and order.getOrderRating() == '0'}">
                          <form method="post" action="ordersStatus-servlet">
                            <input class="input-group-sm" type="number" name="changeRating" min="1" max="10" value="9" size="2">
                            <button class="btn btn-outline-dark" type="submit" name="orderId" value="${order.getOrderId()}"><fmt:message key="label.changeRating"/></button>
                          </form>
                        </c:if>
                        <c:if test="${sessionScope.get('user').getRole() == 'master'}">
                          <form id="statusForm" onsubmit="return checkStatus(this, '${order.getOrderStatus()}')" action="ordersStatus-servlet" method="post">
                            <select name="changeStatus">
                              <c:if test="${order.getOrderStatus() != 'Finished'}"><option value="Reserved" <c:if test="${order.getOrderStatus() == 'Reserved'}">disabled="disabled" selected="selected"</c:if>><fmt:message key="label.reserved"/></option></c:if>
                                <option value="Finished" <c:if test="${order.getOrderStatus() == 'Finished'}">disabled="disabled" selected="selected"</c:if>><fmt:message key="label.finished"/></option>
                              <c:if test="${order.getOrderStatus() != 'Finished'}"><option value="Canceled" <c:if test="${order.getOrderStatus() == 'Canceled'}">disabled="disabled" selected="selected"</c:if>><fmt:message key="label.canceled"/></option></c:if>
                            </select>
                            <button class="btn btn-outline-dark" type="submit"  name="orderId" value="${order.getOrderId()}"><fmt:message key="label.changeStatus"/></button>
                          </form>
                          <div id="errorStatus" style="color: red"></div>
                        </c:if>
                        <c:if test="${sessionScope.get('user').getRole() == 'admin'}">
                          <form method="post" action="orders-servlet">
                            <c:if test="${order.getOrderStatus() != 'Finished'}">
                              <button class="btn btn-dark" type="submit" name="orderNumber" value="${order.getOrderId()}"><fmt:message key="label.changeTime"/></button>
                            </c:if>
                          </form>
                        </c:if>
                      </td>
                    </tr>
                  </c:forEach>
                </tbody>
              </c:if>
            </table>
          </div>
        </c:if>



        <c:if test="${sessionScope.get('user').getRole() == 'admin' and not empty requestScope.get('usersList') and empty requestScope.get('ordersList') and empty requestScope.get('mastersList')}">
          <div class="text-end p-3">
            <button type="button" onclick="window.location.href = 'NewMaster.html'" class="btn btn-outline-info"> <fmt:message key="label.changeRole"/> </button>
          </div>
        </c:if>

        <c:if test="${not empty requestScope.get('usersList') and sessionScope.get('user').getRole() == 'admin' and empty requestScope.get('mastersList')}">
          <div>
            <table class="table_sort">

              <caption><fmt:message key="label.listUsers"/></caption>

              <thead>
                <tr>
                  <th onclick=""> Id </th>
                  <th> <fmt:message key="label.firstName"/> </th>
                  <th> <fmt:message key="label.lastName"/> </th>
                  <th> <fmt:message key="label.age"/> </th>
                  <th> <fmt:message key="label.sex"/> </th>
                  <th> <fmt:message key="label.phone"/> </th>
                  <th> Email </th>
                  <th> <fmt:message key="label.role"/> </th>
                </tr>
              </thead>

              <tbody>
              <c:forEach var="userList" items="${requestScope.get('usersList')}">

                <tr>
                  <td> ${userList.getId()} </td>
                  <td> ${userList.getFirstName()} </td>
                  <td> ${userList.getLastName()} </td>
                  <td> ${userList.getAge()} </td>
                  <td> ${userList.getSex()} </td>
                  <td> ${userList.getPhone()} </td>
                  <td> ${userList.getEmail()} </td>
                  <td> ${userList.getRole()} </td>

                </tr>
              </c:forEach>
              </tbody>
            </table>
          </div>
        </c:if>
      </div>

  <script src="js/index.js"></script>
  <script src="js/bootstrap.bundle.min.js" > </script>

  </body>
</html>