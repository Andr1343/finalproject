<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Andriy Aftanas">
    <meta name="generator" content="">
    <title>MyProfile</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="css/Registration.css" rel="stylesheet">
</head>

<body class="text-center">
<main class="form-signin w-100 m-auto">
    <form id="regForm" onsubmit="return checkProfile(this)" action="registration-servlet" method="post">
        <img class="mb-4Logo" src="img/Logo2.jpg" alt="" width="300" height="250">
        <h1 class="h3 mb-3 fw-normal">Hello ${sessionScope.get('user').getFirstName()}</h1>

        <div class="form-floating">
            <input type="text" class="form-control" name="firstName" id="firstN" value="${sessionScope.get('user').getFirstName()}">
            <label for="firstN">FirstName</label>
        </div>
        <div class="form-floating">
            <input type="text" class="form-control" name="lastName" id="lastN" value="${sessionScope.get('user').getLastName()}">
            <label for="lastN">LastName</label>
        </div>
        <div class="form-floating">
            <input type="number" class="form-control" min="16" max="99" value="${sessionScope.get('user').getAge()}" name="ages" id="age" placeholder="25">
            <label for="age">Age</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" name="password" id="pass" placeholder="Password">
            <label for="pass">Password</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" name="confirmPassword" id="confPass" placeholder="Confirm Password">
            <label for="confPass">Confirm Password</label>
        </div>
        <div class="checkbox mb-3">
            <label>
                <input type="radio" name="sex" value="Male" checked> Male
            </label>
            <label>
                <input type="radio" name="sex" value="Female"> Female
            </label>
        </div>
        <div id="error" style="color: red"></div>
        <button class="w-100 btn btn-lg btn-primary" type="submit">Update profile</button>
    </form>
    <button onclick="window.location.href = './'" class="btn btn-outline-warning">Return Back</button>
</main>

<script src="js/MyProfile.js" ></script>

</body>
</html>