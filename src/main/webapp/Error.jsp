<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Error page</title>
        <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <style>
            body {
                background: url(<c:if test="${sessionScope.get('error') == 'email'}">img/EmailNotFound.jpg</c:if><c:if test="${sessionScope.get('error') == 'password'}">img/SomethingWentWrong.jpg</c:if><c:if test="${sessionScope.get('error') == 'isUsed'}">img/SomethingWentWrong.jpg</c:if><c:if test="${sessionScope.get('error') == null}">img/SomethingWentWrong2.jpg</c:if>) no-repeat center center fixed;
                background-size: auto;
            }
        </style>
    </head>

    <body>
        <c:if test="${sessionScope.get('error') == 'isUsed'}">
            <div>
                <form action="LogIn.html" style="text-align: center; margin-top: 2%">
                    <div style="color: red; font-size: xx-large; font-weight: bolder">User with the current Email or PhoneNumber has already been created</div> <br>
                    <button class="btn btn-outline-primary" type="submit" style="font-weight: bolder; font-size: x-large">
                        Log In
                    </button>
                </form>
            </div>
        </c:if>

        <c:if test="${sessionScope.get('error') == 'password'}">
            <div>
                <form action="LogIn.html" style="text-align: center; margin-top: 2%">
                    <div style="color: red; font-size: xx-large; font-weight: bolder">Incorrect password</div> <br>
                    <button class="btn btn-outline-warning" type="submit" style="font-weight: bolder; font-size: x-large">
                        Try again
                    </button>
                </form>
            </div>
        </c:if>
    </body>
</html>