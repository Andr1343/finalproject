<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="message"/>

<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Andriy Aftanas">
  <meta name="generator" content="">
  <title>New Service</title>
  <link rel="canonical" href="https://getbootstrap.com/docs/5.2/examples/sign-in/">
  <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/NewService.css" rel="stylesheet">
</head>

  <body class="text-center">
    <main class="form-logIn w-100 m-auto">

      <c:if test="${sessionScope.get('user').getRole() == 'admin'}">
      <form onsubmit="return checkNewService(this)" action="services-servlet" method="post">
        <img class="mb-4Logo" src="img/Logo2.jpg" alt="" width="299" height="250">
        <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
        <div class="form-floating">
          <input type="text" class="form-control" name="serviceName" id="serviceName1">
          <label for="serviceName1"> <fmt:message key="label.nameService"/> </label>
        </div>
        <div class="form-floating">
          <input type="number" class="form-control" name="servicePrice" id="servicePrice1" min="300" value="300">
          <label for="servicePrice1"> <fmt:message key="label.servicePrice"/> </label>
        </div>
        <div id="error" style="color: red"></div>
        <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.addNewService"/> </button>
      </form>
      </c:if>

      <c:if test="${sessionScope.get('user').getRole() == 'master'}">
        <form onsubmit="return checkService(this)" action="services-servlet" method="post">
          <img class="mb-4Logo" src="img/Logo2.jpg" alt="" width="299" height="250">
          <h1 class="h3 mb-3 fw-normal"> <fmt:message key="label.fillDetails"/> </h1>
        <div class="form-floating">
          <select class="form-control" name="serviceName" id="serviceName2">
            <option value="choose" disabled="disabled" selected="selected"> <fmt:message key="label.chooseService"/> </option>
            <c:forEach var="service" items="${sessionScope.get('servicesList')}">
              <option value="${service.getName()}" <c:forEach var="serviceMaster" items="${sessionScope.get('servicesMasterList')}"> <c:if test="${serviceMaster.getName() == service.getName()}"> disabled="disabled" </c:if> </c:forEach>> ${service.getName()} </option>
            </c:forEach>
          </select>
          <label for="serviceName2"> <fmt:message key="label.nameService"/> </label>
        </div>
        <div class="form-floating">
          <input type="number" class="form-control" name="servicePrice" id="servicePrice2" min="300" value="300">
          <label for="servicePrice2"> <fmt:message key="label.servicePrice"/> </label>
        </div>
        <div id="error1" style="color: red"></div>
        <button class="w-100 btn btn-lg btn-primary" type="submit"> <fmt:message key="label.addNewService"/> </button>
      </form>
      </c:if>

      <br>
      <button onclick="window.location.href = 'services-servlet'" class="btn btn-outline-warning"> <fmt:message key="label.cancel"/> </button>
    </main>

  <script src="js/NewService.js" ></script>
  </body>
</html>
